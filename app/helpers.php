<?php

if (!file_exists('getLastSql')) {
    function getLastSql()
    {
        \think\facade\Db::listen(function($sql, $time, $explain){
            // 记录SQL
            echo $sql. ' ['.$time.'s]';
            // 查看性能分析结果
            echo $explain;
        });
    }
}